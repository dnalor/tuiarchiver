/*
Copyright © 2023 r.s.u.
This file is part of tuiarchiver
*/
package main

import "nexus0.net/go/tuiarchiver/cmd"

func main() {
	cmd.Execute()
}
