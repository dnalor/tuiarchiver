## TUI Archiver
is a terminal application to list / manage archives. While it can be used directly from the command line, it has some features making it useful as a helper for TUI file managers like vifm, ranger, joshito and lf.

### Features
**functions**

- fullscreen TUI to browse, view and extract members
- list archives in the terminal (table or tree)
- extract members from the command line
- preview regular / compressed files on the command line

**general**

- supports tar.[gz|zst|xz|bz2], zip, rar
- file preview supports syntax highlighting, markdown and HTML rendering, PDF text extraction, images
- file icons, colors, themes
- single binary, no dependencies
- shell completion

More information on the [homepage](https://www.nexus0.net/pub/sw/tuiarchiver/)
