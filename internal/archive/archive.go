/*
Copyright © 2023 r.s.u.
This file is part of tuiarchiver
*/
package archive

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"
	"regexp"
	"golang.org/x/term"

	"github.com/muesli/termenv"
	"github.com/charmbracelet/lipgloss"
	humanize "github.com/dustin/go-humanize"
	"github.com/jedib0t/go-pretty/v6/list"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
	"github.com/mistakenelf/teacup/icons"
	archiver "github.com/mholt/archiver/v4"
	log "github.com/sirupsen/logrus"
)

const (
	ansiReset = "\033[0m"
)

var listStyleNone = list.Style{
	Format:           text.FormatDefault,
	CharItemSingle:   "",
	CharItemTop:      "",
	CharItemFirst:    "",
	CharItemMiddle:   "",
	CharItemVertical: "  ",
	CharItemBottom:   "",
	CharNewline:      "\n",
	LinePrefix:       "",
	Name:             "StyleDefault",
}

type NodeDir struct {
	fs.FileInfo
	Archname string
	Basename string
	Mode     fs.FileMode
	ModTime  time.Time
	Parent   *NodeDir
	Subdirs  []*NodeDir
	Files    []*NodeFile
}

type NodeFile struct {
	fs.FileInfo
	Archname string
	Basename string
	Parent   *NodeDir
	Size     uint64
	Mode     fs.FileMode
	ModTime  time.Time
}

type ListingParms struct {
	ListType  *string `koanf:"type"`
	SortOrder *string `koanf:"sort-order"`
	Theme     *string `koanf:"table-style"`
	NoIcons   bool `koanf:"no-icons"`
	NoHeaders bool `koanf:"no-headers"`
	TreeStyle *string `koanf:"tree-style"`
	Columns   int
	MaxDepth  int
	MaxFiles  int
}

func (p *ListingParms) AutoTermSize() {
	if p.Columns == 0 {
		width, _, err := term.GetSize( int(os.Stdout.Fd()))
		if err == nil { 
			p.Columns = width
		} else {
			p.Columns = 132
		}
	}
}

func min(a, b int64) int64 {
	if a < b {
		return a
	}
	return b
}

func (nodes *NodeDir) findDirNode(dirs []string, addMissing bool) (*NodeDir, error) {
	currnodes := nodes.Subdirs
	foundNode := nodes
	found := false

	for _, d := range dirs {
		log.Tracef("** findDirNode: checking dir %s (nodes: %d)", d, len(currnodes))
		found = false
		for _, n := range currnodes {
			if n.Basename == d {
				foundNode = n
				currnodes = n.Subdirs
				log.Tracef("** findDirNode: found ** cn: %+v", currnodes)
				found = true
				break
			}
		}
		if !found {
			log.Tracef("** findDirNode: !found, adding " + d)
			if addMissing {
				node := &NodeDir{
					FileInfo: nil,
					Archname: "",
					Basename: d,
					Parent:   foundNode,
					Subdirs:  []*NodeDir{},
					Files:    []*NodeFile{},
				}
				foundNode.Subdirs = append(foundNode.Subdirs, node)
				foundNode = node
				currnodes = node.Subdirs
				found = true
			} else {
				log.Warnf("** findDirNode: !found !adding " + d)
				return nil, errors.New("not found")
			}
		}
	}
	if found {
		log.Tracef("** findDirNode: final found; currnodes: %d sdirs: %d", len(currnodes), len(foundNode.Subdirs))
		return foundNode, nil
	}
	return nil, errors.New("not found")
}

func (nodes *NodeDir) AddFile(node *NodeFile) bool {
	dirs := strings.Split(filepath.Dir(node.Archname), "/")
	foundNode, err := nodes.findDirNode(dirs, true)
	if err == nil {
		node.Parent = foundNode
		foundNode.Files = append(foundNode.Files, node)
		log.Tracef("** AddFile final dir %s found;  files: %d", foundNode.Basename, len(foundNode.Files))
		return true
	}
	return false
}

func (nodes *NodeDir) AddDir(node *NodeDir) bool {
	dirs := strings.Split(strings.TrimSuffix(node.Archname, "/"), "/")
	foundNode, err := nodes.findDirNode(dirs[0:len(dirs)-1], true)
	if err == nil {
		foundNode.Subdirs = append(foundNode.Subdirs, node)
		log.Tracef("** AddDir final dir %s found;  sdirs: %d", foundNode.Basename, len(foundNode.Subdirs))
		return true
	}
	return false
}

func (nodes *NodeDir) PrintTree(parms ListingParms) {
	l := list.NewWriter()
	switch *parms.TreeStyle {
	case "simple":
		l.SetStyle(listStyleNone)
	case "line":
		l.SetStyle(list.StyleConnectedLight)
	case "md":
		l.SetStyle(list.StyleMarkdown)
	}
	nodes.generateTree(l, parms, 0)
	fmt.Println(l.Render())
}

func (node *NodeDir) generateTree(l list.Writer, parms ListingParms, level int) {
	icon, color := icons.GetIcon(node.Basename, filepath.Ext(node.Basename), "/")
	w := 3
	fc := "  "
	if parms.NoIcons {
		icon = ""
		w = 1
		fc = ""
	}
	//log.Debugf("dir: %s icon: %s color: %s", node.Basename, icon, color)
	fileIcon := lipgloss.NewStyle().Width(w).Render(fmt.Sprintf("%s%s%s", color, icon, fc))
	//fmt.Printf(strings.Repeat("  ", level)+"%s %s%s (l:%d)\n", fileIcon, nodes.Basename, ansiReset, level)
	if level != 0 {
		l.Indent()
		l.AppendItem(fileIcon + node.Basename + ansiReset)
	}
	for _, s := range node.Subdirs {
		s.generateTree(l, parms, level+1)
	}
	l.Indent()
	for _, s := range node.Files {
		icon, color := icons.GetIcon(s.Basename, filepath.Ext(s.Basename), "")
		if parms.NoIcons {
			icon = ""
		}
		fileIcon := lipgloss.NewStyle().Width(w).Render(fmt.Sprintf("%s%s%s", color, icon, fc))
		//fmt.Printf(strings.Repeat("  ", level)+"  %s %s%s [%s] [%s] [%s]\n", fileIcon, s.Basename, ansiReset, humanize.Bytes(s.Size), s.ModTime, s.Mode.String())
		m := fmt.Sprintf(" | %s | %s | %s", humanize.Bytes(s.Size), s.ModTime.Format("2006-01-02 15:04:05"), s.Mode.String())
		m = lipgloss.NewStyle().Faint(true).Render( m)
		s := fmt.Sprintf("%s%s%s%s", fileIcon, s.Basename, ansiReset, m)
		l.AppendItem(s)
	}
	l.UnIndent()
	if level != 0 {
		l.UnIndent()
	}
}

func printFlat(entries map[string]map[string]string, parms ListingParms) (string, error) {
	keys := make([]string, 0, len(entries))
	for name := range entries {
		keys = append(keys, name)
	}
	if *parms.SortOrder == "archive" {
		posComp := func(p1 string, p2 string) bool {
			n1, _ := strconv.Atoi(p1)
			n2, _ := strconv.Atoi(p2)
			return n1 < n2
		}
		sort.Slice(keys, func(i, j int) bool { return posComp(entries[keys[i]]["pos"], entries[keys[j]]["pos"]) })
	} else {
		sort.Strings(keys)
	}

	t := table.NewWriter()
	t.SetAutoIndex(false)
	t.SetOutputMirror(os.Stdout)
	if ! parms.NoHeaders {
		t.AppendHeader(table.Row{"name", "size", "date", "perm"})
	}

	for _, name := range keys {
		var ind string
		if entries[name]["type"] == "dir" {
			ind = "/"
		} else {
			ind = ""
		}
		icon, color := icons.GetIcon(entries[name]["basename"], filepath.Ext(entries[name]["basename"]), ind)
		fileIcon := ""
		if !parms.NoIcons {
			fileIcon = lipgloss.NewStyle().Width(2).Render(fmt.Sprintf("%s%s", color, icon))
		} else {
			fileIcon = lipgloss.NewStyle().Width(1).Render(fmt.Sprintf("%s%s", color, ""))
		}

		//log.Debugf( "%s %s %s %s", strings.TrimSpace(name), entries[name]["size"], entries[name]["modTime"], entries[name]["mode"])
		t.AppendRow(table.Row{fileIcon + strings.TrimSpace(name) + ansiReset, entries[name]["size"], entries[name]["modTime"], entries[name]["mode"]})
	}

	t.SetColumnConfigs([]table.ColumnConfig{
		{
			Name:  "size",
			Align: text.AlignRight,
		},
	})

	t.SetAllowedRowLength(parms.Columns)
	var r string
	switch *parms.Theme {
	case "bright":
		t.SetStyle(table.StyleColoredBright)
		r = t.Render()
	case "dark":
		t.SetStyle(table.StyleColoredDark)
		r = t.Render()
	case "simple":
		t.SetStyle(table.StyleLight)
		t.Style().Options.DrawBorder = false
		t.Style().Options.SeparateColumns = false
		r = t.Render()
	case "md":
		r = t.RenderMarkdown()	
	case "csv":
		r = t.RenderCSV()
	case "html":
		t.Style().HTML = table.HTMLOptions{
			EmptyColumn: "&nbsp;",
			EscapeText:  true,
			Newline:     "<br/>",
		}
		t.RenderHTML()
	default:
		e := fmt.Sprintf("invalid table style: %s", *parms.Theme)
		log.Error( e)
		return r, errors.New(e)
	}
	return r,nil
}

func ExtractMembers( ex archiver.Extractor, archpath string, members []string, regex *string, destdir string, fullPaths bool) ( string, error) {

	arch, err := os.Open(archpath)
	if err != nil {
		log.Fatal(err.Error())
		return "", err
	}
	defer arch.Close()
	numExtractions := 0
	numMembers := 0
	var messages []string
	dirsCreated := make(map[string]bool)

	var matchMember *regexp.Regexp
	if regex != nil {
		if matchMember, err = regexp.Compile( *regex); err != nil {
			return fmt.Sprintf("invalid regex: %s", *regex), err
		}
	}

	handler := func(ctx context.Context, f archiver.File) error {
		numMembers++
		if matchMember != nil && !matchMember.MatchString( f.NameInArchive) {
			log.Debugf("skipped %s (!regex)", f.NameInArchive)
			return nil
		}

		if f.IsDir() {
			d := filepath.Clean( filepath.Join( destdir, f.NameInArchive))
			err := os.MkdirAll( d, 0750)
			if err != nil && !os.IsExist(err) {
				log.Fatal(err)
				messages = append(messages, "failed to create: " + d)
				return err
			}
			numExtractions++
			dirsCreated[ d] = true
			return nil
		}

		var rc io.ReadCloser
		if rc, err = f.Open(); err != nil {
			log.Error(err.Error())
			return err
		}
		defer rc.Close()
//		sz := f.FileInfo.Size()
		var bytes []byte
		//log.Debugf("file %s fsize: %d bytes: %d", f.NameInArchive, sz, len( bytes))
		//var num int
		if bytes, err = io.ReadAll( rc); err != nil {
			messages = append(messages, fmt.Sprintf("read error on %s: %v", f.NameInArchive, err))
			log.Warnf("rc.Read: %v", err.Error())
			return err
		} 
	
		dd := filepath.Clean( filepath.Join( destdir, filepath.Dir( f.NameInArchive)))
		log.Debugf("destDir: %s", dd)

		if !dirsCreated[ dd] {
			err := os.MkdirAll( dd, 0750)
			if err != nil && !os.IsExist(err) {
				log.Fatal(err)
				messages = append(messages, "failed to create: "+dd)
				return err
			} else {
				dirsCreated[ dd] = true
			}
		}

		destfile := filepath.Join( dd, f.FileInfo.Name())
		log.Debugf("destFile: %s", destfile)
		if _, err := os.Stat( destfile); os.IsNotExist(err) {
			err = os.WriteFile( destfile, bytes, f.FileInfo.Mode())
			if err != nil {
				messages = append(messages, "failed to write: "+destfile)
				log.Error(err)
				return err
			}
			numExtractions++
		}
		return nil
	}
	err = ex.Extract(context.Background(), arch, members, handler)
	if err != nil {
		log.Warn(err.Error())
		return "",err
		}
	return fmt.Sprintf("extracted %d of %d (errors: %d)", numExtractions, numMembers, len(messages)), nil
}

func ReadMember( ex archiver.Extractor, archpath string, member string, maxSize int64) ([]byte, error) {
	var result []byte
	m := []string{ member}
	arch, err := os.Open(archpath)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer arch.Close()
	handler := func(ctx context.Context, f archiver.File) error {
		var rc io.ReadCloser
		if rc, err = f.Open(); err != nil {
			log.Warn(err.Error())
			return err
		}
		defer rc.Close()
		sz := f.FileInfo.Size()
		if maxSize != 0 {
			sz = min( f.FileInfo.Size(), maxSize)
			log.Debugf("ReadMember: size: %d max: %d", sz, maxSize)
		}
		var bytes []byte
		if bytes, err = io.ReadAll( rc); err != nil {
			log.Errorf( "rc.Read %v", err.Error())
			return err
		}
		result = bytes // string(bytes[:])
		return nil
	}
	err = ex.Extract(context.Background(), arch, m, handler)
	if err != nil {
		log.Warn(err.Error())
		return nil, err
		}
	return result, nil
}

func ReadArchive(fname string) (archiver.Extractor, *NodeDir, map[string]map[string]string, error) {

	arch, err := os.Open(fname)
	if err != nil {
		log.Error(err.Error())
		return nil, nil, nil, err
	}
	defer arch.Close()

	nodes := &NodeDir{
		FileInfo: nil,
		Archname: "/",
		Basename: "/",
		Parent:   nil,
		Subdirs:  []*NodeDir{},
		Files:    []*NodeFile{},
	}
	allEntries := map[string]map[string]string{}
	entryCounter := 0

	format, arch_s, err := archiver.Identify(fname, arch)
	if err != nil && !errors.Is(err, archiver.ErrNoMatch) {
		log.Error(err.Error())
		return nil, nil, nil, err
	}

	if ex, ok := format.(archiver.Extractor); ok {

		handler := func(ctx context.Context, f archiver.File) error {
			archname := f.NameInArchive
			basename := f.FileInfo.Name()
			fsize := uint64(f.FileInfo.Size())
			modTime := f.FileInfo.ModTime()
			mode := f.FileInfo.Mode()
			n := map[string]string{"basename": basename} // "archname": f.NameInArchive,
			if f.FileInfo.IsDir() {
				//log.Debugf("  dir: %s (%s)", f.NameInArchive, f.FileInfo.Name())
				nodes.AddDir(&NodeDir{Archname: archname, FileInfo: f.FileInfo, Basename: basename, ModTime: modTime, Mode: mode})
				n["type"] = "dir"
			} else {
				//log.Debugf("  file: %s (%s)", f.NameInArchive, f.FileInfo.Name())
				nodes.AddFile(&NodeFile{Archname: archname, FileInfo: f.FileInfo, Basename: basename, Size: fsize, ModTime: modTime, Mode: mode})
				n["type"] = "file"
				n["size"] = humanize.Bytes(uint64(f.FileInfo.Size()))
			}
			n["modTime"] = modTime.Format("2006-01-02 15:04:05")
			n["mode"] = mode.String()
			n["pos"] = strconv.Itoa(entryCounter)
			entryCounter++
			allEntries[archname] = n
			return nil
		}

		err := ex.Extract(context.Background(), arch_s, nil, handler)
		if err != nil {
			return nil, nil, nil, err
		}
		return ex, nodes, allEntries, err
	}
	return nil, nil, nil, err
}

func ListArchive(fname string, parms ListingParms) error {
	//log.Debugf("type: %s cols: %d", *parms.ListType, parms.Columns)

	_, nodes, allEntries, err := ReadArchive(fname)
	if err != nil {
		log.Fatalf(err.Error())
		return err
	}

	lipgloss.SetColorProfile( termenv.EnvColorProfile())
	log.Debugf("ColorProfile: %v", lipgloss.ColorProfile())

	if *parms.ListType == "tree" {
		nodes.PrintTree(parms)
	} else if *parms.ListType == "table" {
		printFlat(allEntries, parms)
	} else {
		return errors.New( fmt.Sprintf( "unknown type %s", *parms.ListType))
	}
	return nil
}
