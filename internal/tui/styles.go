/*
Copyright © 2023 r.s.u.
This file is part of tuiarchiver
*/
package tui

import (
	"github.com/charmbracelet/lipgloss"
	"github.com/evertras/bubble-table/table"
)

var (
	tableCustomBorder = table.Border{
		Top:    "─",
		Left:   "│",
		Right:  "│",
		Bottom: "─",

		TopRight:    "╮",
		TopLeft:     "╭",
		BottomRight: "╯",
		BottomLeft:  "╰",

		TopJunction:    "╥",
		LeftJunction:   "├",
		RightJunction:  "┤",
		BottomJunction: "╨",
		InnerJunction:  "╫",

		InnerDivider: "║",
	}
	tableCustomBorderThin = table.Border{
		Top:    "─",
		Left:   "│",
		Right:  "│",
		Bottom: "─",

		TopRight:    "┐",
		TopLeft:     "┌",
		BottomRight: "┘",
		BottomLeft:  "└",

		TopJunction:    "┬",
		LeftJunction:   "├",
		RightJunction:  "┤",
		BottomJunction: "┴",
		InnerJunction:  "┼",

		InnerDivider: "│",
	}

	lgColorSubtle    = lipgloss.AdaptiveColor{Light: "#D9DCCF", Dark: "#383838"}
	lgColorHighlight = lipgloss.AdaptiveColor{Light: "#874BFD", Dark: "#7D56F4"}
	lgColorSpecial   = lipgloss.AdaptiveColor{Light: "#43BF6D", Dark: "#73F59F"}
	lgColorSnazzyBlue = lipgloss.Color("#57C7FF")
	lgColorSnazzyYellow = lipgloss.Color("#F3F99D")
	lgColorSnazzyGreen = lipgloss.Color("#5AF78E")
	lgColorSnazzyCyan = lipgloss.Color("#9AEDFE")
	lgColorSnazzyRed = lipgloss.Color("#FF5C57")

	lgSatusBarStyle = lipgloss.NewStyle().
		Foreground(lipgloss.AdaptiveColor{Light: "#343433", Dark: "#C1C6B2"}).
		Background(lipgloss.AdaptiveColor{Light: "#D9DCCF", Dark: "#353533"})

	lgSpinnerStyle = lipgloss.NewStyle().Foreground(lgColorSnazzyRed)

)


