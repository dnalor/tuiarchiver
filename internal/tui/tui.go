/*
Copyright © 2023 r.s.u.
This file is part of tuiarchiver
*/
package tui

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"os"
	"path/filepath"
	"golang.org/x/term"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/bubbles/spinner"
	"github.com/charmbracelet/bubbles/textinput"
	"github.com/charmbracelet/lipgloss"
	"github.com/evertras/bubble-table/table"
	"github.com/mistakenelf/teacup/icons"
	"github.com/mistakenelf/teacup/help"
	"github.com/muesli/reflow/truncate"

	log "github.com/sirupsen/logrus"
	archiver "github.com/mholt/archiver/v4"

	archive "nexus0.net/go/tuiarchiver/internal/archive"
)

const (
	appStateMain uint = iota
	appStateViewFile
	appStateExtract
	appStateFolderSelect
	appStateMainFilter
	appStateShowHelp

	columnKeyName = "name"
	columnKeyMemberName = "member"
	columnKeyType = "type"
	columnKeySize = "size"
	columnKeyModTime = "mtime"
	columnKeyMode  = "mode"
	columnKeyNode = "node"

	ansiReset = "\033[0m"
	termRowsReserve = 7
	maxViewFileSize = 10*1024*1024
	maxCacheSize = 5*1024*1024
)

type TuiParms struct {
	Columns uint
	Rows uint
	NoIcons bool `koanf:"no-icons"`
	DefaultExtractPath *string `koanf:"extract-path"`
	MaxViewFileSize uint64
	ViewSyntaxTheme *string `koanf:"viewer-syntax-theme"`
}

type Model struct {
	parms TuiParms
	appState uint
	archPath string
	archiver.Extractor
	allNodes *archive.NodeDir
	allEntries map[string]map[string]string
	memberCache map[string] []byte
	archTable table.Model
	filterTextInput textinput.Model
	currentFilter string
	spinner spinner.Model
	viewer Viewer
	textInput textinput.Model
	folderSelector FolderSelectBubble
	helpScreen help.Model
	opInProgress bool
	msg string
}

type readArchiveMsg struct {
	archPath string
	archiver.Extractor
	allNodes *archive.NodeDir
	allEntries map[string]map[string]string
	err error
}

type opProgressMsg struct {
	progress uint
}

type viewMemberMsg struct {
	membername string
	content []byte
	err error
}

type renderMemberMsg struct {
	membername string
	err error
}

type extractMembersMsg struct {
	msg string
	err error
}

func (p *TuiParms) AutoTermSize() {
	if p.Columns == 0 || p.Rows == 0 {
		width, height, err := term.GetSize( int(os.Stdout.Fd()))
		if err == nil { 
			if p.Columns == 0 {
				p.Columns = uint(width)
			}
			if p.Rows == 0 {
				p.Rows = uint(height)
			}
		} else {
			if p.Columns == 0 {
				p.Columns = 132
			}
			if p.Rows == 0 {
				p.Rows = 60
			}
		}
	}
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func (m *Model) MemberCachePut( key string, val []byte) {
	if len( val) < maxCacheSize {
		m.memberCache[ key] = val
	}
}

func (m *Model) MemberCacheGet( key string) ([]byte, bool) {
	val, ok := m.memberCache[ key]
	return val, ok
}

func (m *Model) GenerateRows() {
	m.archTable = m.archTable.WithRows(generateRowsFlat(m.allEntries, m.parms.NoIcons))
}

func (m *Model) generateHeaderbar() string {
	switch m.appState {
		case appStateMainFilter:
			return m.filterTextInput.View()
		case appStateExtract:
			return m.textInput.View()
		case appStateFolderSelect:
			return ""
		default:
			tr := ""
			s1 := lipgloss.NewStyle().Bold(true). Foreground(lipgloss.Color("9"))
			s2 := lipgloss.NewStyle().Bold(false).PaddingRight(1)
			sb := strings.Builder{}
			if m.opInProgress {
				sb.WriteString(m.spinner.View()+"  ")
			}
			sb.WriteString( lipgloss.NewStyle().Faint( true).Render("│ "))
			txt := []string{ "quit", "view", "extract", "/ filter", "folders"}
			for _, t := range( txt) {
				sb.WriteString( s1.Render(t[:1]))
				sb.WriteString( s2.Render(t[1:]))
				sb.WriteString( lipgloss.NewStyle().Faint( true).Render("│ "))
			}
			if m.currentFilter != "" {
				sb.WriteString( "\uf0b0  ["+m.currentFilter+"]")
			}
			tl := sb.String()
			ll := lipgloss.Width( tl) + 2

			if m.msg != "" {
				tr = truncate.StringWithTail(m.msg, m.parms.Columns - uint(ll), "…")
			}
			lr := lipgloss.Width( tr)
			return lipgloss.NewStyle().Width( int(m.parms.Columns)-lr).Align(lipgloss.Left).Render( tl)+lipgloss.NewStyle().Foreground( lgColorSnazzyYellow).Render( tr)
	}
}

func createHelpScreen( parms TuiParms) help.Model {
	entries := 	[]help.Entry{
			{Key: "q/\u2190", Description: "Quit"},
			{Key: "\u2191/j", Description: "Move up"},
			{Key: "\u2193/k", Description: "Move down"},
			{Key: "v/(\u2192\u2190)", Description: "toggle viewer"},
			{Key: "<pg \u2191/\u2193>", Description: "prev/next page"},
			{Key: "<space>", Description: "select member"},
			{Key: "e", Description: "extract member(s)"},
			{Key: "\u241b", Description: "cancel"},
			{Key: "/", Description: "filter"},
			{Key: "f", Description: "folder filter"},
			{Key: "r/R", Description: "(viewer) display rendered / raw"},
			{Key: "alt+a", Description: "(extract) set path to archive dir"},
			{Key: "alt+w", Description: "(extract) set path to working dir"},
			{Key: "alt+h", Description: "(extract) set path to home dir"},
			{Key: "alt+t", Description: "(extract) set path to temp dir"},
	}
	hs := help.New(
		true,
		false,
		"Help",
		help.TitleColor{
				Background: lipgloss.AdaptiveColor{Light: "7", Dark: "0"},
				Foreground: lipgloss.AdaptiveColor{Light: "2", Dark: "2"},
		},
		lipgloss.AdaptiveColor{Light: "#000000", Dark: "#ffffff"},
		entries,
	)
	hs.SetSize( int(parms.Columns)-1, min( int(parms.Rows)-2, len( entries)+5))
	return hs
}

func createTextInput( parms TuiParms) textinput.Model {
	ti := textinput.New()
	ti.Width = int( parms.Columns)-4
	return ti
}

func generateRowsFlat(entries map[string]map[string]string, noIcons bool) []table.Row {
	keys := make([]string, 0, len(entries))
	for name := range entries {
		keys = append(keys, name)
	}
	sortOrder := "archive"
	if sortOrder == "archive" {
		posComp := func(p1 string, p2 string) bool {
			n1, _ := strconv.Atoi(p1)
			n2, _ := strconv.Atoi(p2)
			return n1 < n2
		}
		sort.Slice(keys, func(i, j int) bool { return posComp(entries[keys[i]]["pos"], entries[keys[j]]["pos"]) })
	} else {
		sort.Strings(keys)
	}

	rows := []table.Row{}
	for _, name := range keys {
		var ind string
		if entries[name]["type"] == "dir" {
			ind = "/"
		} else {
			ind = ""
		}
		icon, color := icons.GetIcon(entries[name]["basename"], filepath.Ext(entries[name]["basename"]), ind)
		fileIcon := ""
		if !noIcons {
			fileIcon = lipgloss.NewStyle().Width(2).Render(fmt.Sprintf("%s%s", color, icon))
		} else {
			fileIcon = lipgloss.NewStyle().Width(1).Render(fmt.Sprintf("%s%s", color, ""))
		}
		// FIXME: make dir rows non-selectable
		rows = append(rows, table.NewRow(table.RowData{
			columnKeyName: fileIcon + strings.TrimSpace(name) + ansiReset,
			columnKeyType: entries[name]["type"],
			columnKeySize: entries[name]["size"], 
			columnKeyModTime: entries[name]["modTime"], 
			columnKeyMode: entries[name]["mode"],
			columnKeyMemberName: name,
		}))
	}
	return rows
}

func createArchTable(parms TuiParms) table.Model {
	return  table.New([]table.Column{
			table.NewFlexColumn(columnKeyName, " name", 13).
			WithStyle( lipgloss.NewStyle().Align(lipgloss.Left)).WithFiltered(true),
			table.NewColumn(columnKeySize, "size", 10),
			table.NewColumn(columnKeyModTime, "time", 20).WithFiltered(true),
			table.NewColumn(columnKeyMode, "mode", 10),
			//table.NewColumn(columnKeyType, "type", 5),
		}).
		WithTargetWidth(int(parms.Columns)).
		WithMaxTotalWidth(int(parms.Columns)).
		WithHorizontalFreezeColumnCount(1).
		HeaderStyle(lipgloss.NewStyle().Foreground(lgColorSnazzyCyan).Bold(true)).
		Border( tableCustomBorderThin).
		WithSelectedText(" ", "✓").
		WithPageSize( int(parms.Rows) - termRowsReserve).
		SelectableRows(true).
		Focused(true).
		Filtered(true)
}

func (m *Model) updateTableFooter() {
	sel := ""
	numSel :=  len(m.archTable.SelectedRows())
	if numSel > 0 {
		sel = fmt.Sprintf(" (# %d)", numSel)
	}
	footerTextRight := lipgloss.NewStyle().Foreground( lgColorSnazzyYellow).Render( fmt.Sprintf( "[%d/%d]", m.archTable.CurrentPage(), m.archTable.MaxPages()))
	footerTextLeft := fmt.Sprintf( "%s%s", lipgloss.NewStyle().Foreground( lgColorSnazzyBlue).Render( m.archPath), sel)
	footerText := lipgloss.NewStyle().Width( int(m.parms.Columns)-lipgloss.Width(footerTextRight)-2).Align(lipgloss.Left).Render( footerTextLeft)+footerTextRight

	m.archTable = m.archTable.WithStaticFooter(footerText)
}

func NewModel( archPath string, parms TuiParms) Model {
	sp := spinner.New()
	sp.Spinner = spinner.Points
	sp.Style = lgSpinnerStyle
	fti := textinput.New()
	fti.Placeholder = "filter by name/time"
	return Model{
		memberCache: make(map[string][]byte, 10),
		appState: appStateMain,
		parms: parms,
		archPath: archPath,
		spinner: sp,
		textInput: createTextInput( parms),
		archTable: createArchTable( parms),
		filterTextInput: fti,
		helpScreen: createHelpScreen( parms),
	}
}

func (m Model) Init() tea.Cmd {
	arCmd := tea.Sequence( opProgressCmd(0), readArchiveCmd( m.archPath), opProgressCmd(100))
	return tea.Batch(tea.EnterAltScreen, m.spinner.Tick, arCmd)
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var (
		cmd  tea.Cmd
		cmds []tea.Cmd
	)

	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		log.Debugf("WindowSizeMsg: w: %d h: %d (curr: w: %d h: %d)", msg.Width, msg.Height, m.parms.Columns, m.parms.Rows)
		if m.parms.Columns != uint( msg.Width) || m.parms.Rows != uint( msg.Height) {
			m.parms.Columns = uint( msg.Width)
			m.parms.Rows = uint( msg.Height)
			//log.Debug("rebuiding table")
			m.archTable = createArchTable( m.parms)
			m.GenerateRows()
			m.updateTableFooter()
		}
	case tea.KeyMsg:
		//log.Debug("key: " + msg.String())
		switch msg.String() {
		case "ctrl+c":
			cmds = append(cmds, tea.Quit)
		case "q","left":
			switch m.appState {
				case appStateMain:
					cmds = append(cmds, tea.Quit)
				case appStateViewFile:
					m.appState = appStateMain
					m.viewer = nil
				case appStateExtract:
					m.textInput, cmd = m.textInput.Update(msg)
					cmds = append(cmds, cmd)
				case appStateMainFilter:
					m.filterTextInput, cmd = m.filterTextInput.Update(msg)
					cmds = append(cmds, cmd)
			}
		case "esc":
			switch m.appState {
				case appStateExtract:
					m.appState = appStateMain
					m.textInput.Blur()
				case appStateViewFile:
					m.appState = appStateMain
					m.viewer = nil
				case appStateShowHelp:
					m.helpScreen.SetIsActive( false)
					m.appState = appStateMain
				case appStateMainFilter:
					m.filterTextInput.SetValue( m.currentFilter)
					m.filterTextInput.Blur()
					m.appState = appStateMain
				case appStateFolderSelect:
					m.appState = appStateMain
			}
		case "enter":
			switch m.appState { 
				case appStateExtract:
					log.Debugf("appStateExtract: input: %s", m.textInput.Value())
					m.textInput.Blur()
					m.appState = appStateMain
					var members []string
					for _, r := range m.archTable.SelectedRows() {
						if r.Data[ columnKeyType].(string) == "file" {
							members = append(members, r.Data[ columnKeyMemberName].(string))
							log.Debugf("row memb: %s", r.Data[ columnKeyMemberName].(string))
						}
					}
					xc := extractMembersCmd( m.Extractor, m.archPath, members, m.textInput.Value(), true)
					cmd := tea.Sequence( opProgressCmd(0), xc, opProgressCmd(100))
					cmds = append(cmds, cmd)
					m.msg = fmt.Sprintf("extracting %d members", len(members))
				case appStateMainFilter: //appStateMain && m.filterTextInput.Focused() {
					log.Debugf("filter: %s", m.filterTextInput.Value())
					m.filterTextInput.Blur()
					m.currentFilter = m.filterTextInput.Value()
					m.archTable = m.archTable.WithFilterInputValue( m.currentFilter)
					m.archTable.Focused( true)
					m.appState = appStateMain
				case appStateFolderSelect:
					if si, err := m.folderSelector.SelectedItem(); err == nil {
						log.Debugf("appStateFolderSelect: %s", si)
						m.currentFilter = si
						m.archTable = m.archTable.WithFilterInputValue( m.currentFilter)
					}
					m.archTable.Focused( true)
					m.appState = appStateMain
			}
		case "v","right":
			switch m.appState {
				case appStateMain:
					if m.archTable.HighlightedRow().Data[ columnKeyType] != nil && m.archTable.HighlightedRow().Data[ columnKeyType].(string) == "file" {
						rd := m.archTable.HighlightedRow().Data[ columnKeyMemberName].(string)
						log.Debugf("appStateMain view: %v", rd)
						vc := viewMemberCmd( m, string(rd))
						cmd := tea.Sequence( opProgressCmd(0), vc, opProgressCmd(100))
						cmds = append(cmds, cmd)
					}
				case appStateViewFile:
					m.appState = appStateMain
					m.viewer = nil
				case appStateExtract:
					m.textInput, cmd = m.textInput.Update(msg)
					cmds = append(cmds, cmd)
				case appStateMainFilter:
					m.filterTextInput, cmd = m.filterTextInput.Update(msg)
					cmds = append(cmds, cmd)
			}
		case "R", "r":
			switch m.appState {
				case appStateViewFile:
					if  m.viewer != nil {
						if msg.String() == "R" {
							m.viewer.ViewRawContent()
						} else {
							m.viewer.ViewRenderedContent()
						}
					}
				case appStateExtract:
					m.textInput, cmd = m.textInput.Update(msg)
					cmds = append(cmds, cmd)
				case appStateMainFilter:
					m.filterTextInput, cmd = m.filterTextInput.Update(msg)
					cmds = append(cmds, cmd)
			}
		case "e":
			switch m.appState {
				case appStateMain: 
					log.Debug("enter appStateExtract")
					if len(m.archTable.SelectedRows()) > 0 {
						m.textInput.Prompt = fmt.Sprintf("extract %d member(s) to: ", len(m.archTable.SelectedRows()))
					} else {
						m.textInput.Prompt = "extract all members to: "
					}
					var dd string
					if *m.parms.DefaultExtractPath == "" {
						dd = filepath.Dir( m.archPath)
					} else {
						dd = *m.parms.DefaultExtractPath
					}
					m.textInput.SetValue( dd)
					m.textInput.Focus()
					cmds = append(cmds, textinput.Blink)
					m.appState = appStateExtract
				case appStateExtract:
					m.textInput, cmd = m.textInput.Update(msg)
					cmds = append(cmds, cmd)
				case appStateMainFilter:
					m.filterTextInput, cmd = m.filterTextInput.Update(msg)
					cmds = append(cmds, cmd)
			}
		case "alt+a":
			if m.appState == appStateExtract {
				d := filepath.Dir( m.archPath)
				m.textInput.SetValue( d)
				m.textInput.SetCursor( len(d))
			}
		case "alt+t":
			if m.appState == appStateExtract {
				d := os.TempDir()
				m.textInput.SetValue( d)
				m.textInput.SetCursor( len(d))
			}
		case "alt+h":
			if m.appState == appStateExtract {
				if d, err := os.UserHomeDir(); err == nil {
					m.textInput.SetValue( d)
					m.textInput.SetCursor( len(d))
				}
			}
		case "alt+w":
			if m.appState == appStateExtract {
				if d, err := os.Getwd(); err == nil {
					m.textInput.SetValue( d)
					m.textInput.SetCursor( len(d))
				}
			}
		case "/":
			switch m.appState {
				case appStateMain:
					m.appState = appStateMainFilter
					m.filterTextInput.Focus()
				case appStateExtract:
					m.textInput, cmd = m.textInput.Update(msg)
					cmds = append(cmds, cmd)
				case appStateMainFilter:
					m.filterTextInput, cmd = m.filterTextInput.Update(msg)
					cmds = append(cmds, cmd)
			}
		case "f":
			switch m.appState {
				case appStateMain:
					if ! m.folderSelector.IsEmpty() {
						m.appState = appStateFolderSelect
					} else {
						m.msg = "no folders"
					}
				case appStateExtract:
					m.textInput, cmd = m.textInput.Update(msg)
					cmds = append(cmds, cmd)
				case appStateMainFilter:
					m.filterTextInput, cmd = m.filterTextInput.Update(msg)
					cmds = append(cmds, cmd)
			}
		case "f1":
			if m.appState == appStateMain {
				m.helpScreen.SetIsActive( true)
				m.appState = appStateShowHelp
			} else if m.appState == appStateShowHelp {
				m.helpScreen.SetIsActive( false)
				m.appState = appStateMain
			}
		default:
			m.msg = ""
			switch m.appState {
				case appStateMain:
					m.archTable, cmd = m.archTable.Update(msg)
					cmds = append(cmds, cmd)
					m.updateTableFooter()
				case appStateViewFile:
					cmd = m.viewer.Update( msg)
					cmds = append(cmds, cmd)
				case appStateExtract:
					m.textInput, cmd = m.textInput.Update(msg)
					cmds = append(cmds, cmd)
				case appStateMainFilter:
					m.filterTextInput, cmd = m.filterTextInput.Update(msg)
					cmds = append(cmds, cmd)
				case appStateFolderSelect:
					m.folderSelector, cmd = m.folderSelector.Update( msg)
					cmds = append(cmds, cmd)
				case appStateShowHelp:
					m.helpScreen, cmd = m.helpScreen.Update( msg)
					cmds = append(cmds, cmd)
			}
		}
	case readArchiveMsg:
		if msg.err == nil {
			m.allEntries = msg.allEntries
			m.allNodes = msg.allNodes
			m.archPath = msg.archPath
			m.Extractor = msg.Extractor
			m.GenerateRows()
			m.msg = fmt.Sprintf("%d members", len(m.allEntries))
			m.archTable, cmd = m.archTable.Update(msg)
			m.folderSelector = NewFolderSelectBubble( m.allNodes, int(m.parms.Columns), int(m.parms.Rows))
			if m.allNodes != nil && len( m.allNodes.Subdirs) > 0 {
				m.folderSelector.InitList()
			} else {
				log.Debug("readArchiveMsg: allNodes == nil")
			}
		} else {
			m.msg = fmt.Sprintf("error: %v", msg.err)
		}
		cmds = append(cmds, cmd)
		m.updateTableFooter()
	case viewMemberMsg:
		//log.Debugf("viewMemberMsg: content; %s", msg.content)
		if msg.err == nil && msg.content != nil {
			m.viewer = NewTextViewer( msg.membername, int(m.parms.Columns), int(m.parms.Rows) - termRowsReserve, *m.parms.ViewSyntaxTheme)
			rc := renderMemberCmd( msg.membername, m.viewer, msg.content)
			cmd := tea.Sequence( opProgressCmd(0), rc, opProgressCmd(100))
			cmds = append(cmds, cmd)
		} else {
			m.msg = "error extracting: "+m.archPath
			m.viewer = nil
			if  msg.err != nil {
				log.Errorf( "viewMemberMsg: error %v", msg.err)
			} else {
				log.Error( "viewMemberMsg: error: no content")
			}
		}
	case renderMemberMsg:
		if msg.err == nil {
			m.viewer.ViewRenderedContent()
			m.appState = appStateViewFile
		} else {
			m.msg = "error viewing "+msg.membername
			m.viewer = nil
		}
	case extractMembersMsg:
		log.Debugf("extractMembersMsg: msg: %s err: %v", msg.msg, msg.err)
		if msg.msg != "" {
			m.msg = msg.msg
		}
		if msg.err == nil {
		// FIXME
			m.GenerateRows()
		} else {
			if msg.msg == "" {
				m.msg = msg.err.Error()
			}
		}
	case opProgressMsg:
		if msg.progress < 100 {
			m.opInProgress = true
			cmds = append(cmds, m.spinner.Tick)
		} else {
			m.opInProgress = false
		}
	case spinner.TickMsg:
		if m.opInProgress {
			m.spinner, cmd = m.spinner.Update(msg)
			cmds = append(cmds, cmd)
		}
	default:
		//log.Trace("Update: default")
		switch m.appState {
			case appStateMain:
				m.updateTableFooter()
			case appStateExtract:
				m.textInput, cmd = m.textInput.Update(msg)
				cmds = append(cmds, cmd)
		}
	}

	return m, tea.Batch(cmds...)
}

func readArchiveCmd( fname string) tea.Cmd {
	return func() tea.Msg {
		 ex, allNodes, allEntries, err := archive.ReadArchive(fname)
		return readArchiveMsg{ fname, ex, allNodes, allEntries, err}
	}
}

func viewMemberCmd( m Model, memberName string) tea.Cmd {
	return func() tea.Msg {
		var content []byte
		var err error
		var ok bool

		if content, ok = m.MemberCacheGet( memberName); !ok {
			log.Tracef( "member %s not in cache", memberName)
			content, err = archive.ReadMember( m.Extractor, m.archPath, memberName, 0) // maxViewFileSize)
			if err == nil {
				m.MemberCachePut( memberName, content)
			}
		} else {
			log.Tracef( "member %s is in cache", memberName)
		}
		return viewMemberMsg{ memberName, content, err}
	}
}

func renderMemberCmd( membername string, viewer Viewer, content []byte) tea.Cmd {
	return func() tea.Msg {
		err := viewer.SetViewportContent( content)
		return renderMemberMsg{ membername: membername, err: err}
	}
}

func extractMembersCmd( ex archiver.Extractor, archName string, members []string, destdir string, fullPaths bool) tea.Cmd {
	return func() tea.Msg {
		msg, err := archive.ExtractMembers( ex, archName, members, nil, destdir, fullPaths)
		return extractMembersMsg{ msg, err}
	}
}

func opProgressCmd( progress uint) tea.Cmd {
	return func() tea.Msg {
		 return opProgressMsg{ progress}
	 }
}

func (m Model) View() string {
	switch m.appState {
		case appStateMain, appStateMainFilter, appStateExtract:
			return lipgloss.JoinVertical(
				lipgloss.Top,
				m.generateHeaderbar(),
				m.archTable.View(),
			)
		case appStateViewFile:
			if m.viewer == nil {
				log.Fatalln("internal error: no viewer")
				return "internal error: no viewer"
			} else {
				return m.viewer.View()
			}
		case appStateShowHelp:
			return m.helpScreen.View()
		case appStateFolderSelect:
			return m.folderSelector.View()
	}
	return lipgloss.NewStyle().Bold( true).Render("fatal internal error")
}

func Show(fname string, parms TuiParms) error {
	p := tea.NewProgram(NewModel( fname, parms), tea.WithAltScreen())
	if _, err := p.Run(); err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}
