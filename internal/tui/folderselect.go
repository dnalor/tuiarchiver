/*
Copyright © 2024 r.s.u.
This file is part of tuiarchiver
*/
package tui

import (
	"fmt"
	"io"
	"errors"

	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/muesli/reflow/truncate"
	log "github.com/sirupsen/logrus"

	archive "nexus0.net/go/tuiarchiver/internal/archive"
)

const (
	fsMavWrapOffset = 2
)

var (
	fsBubbleStyle = lipgloss.NewStyle().
			PaddingLeft(1).
			PaddingRight(1).
			BorderStyle(lipgloss.NormalBorder())
	fsItemStyle = lipgloss.NewStyle().PaddingLeft(2)
	fsSelectedItemStyle = lipgloss.NewStyle().PaddingLeft(0).Bold(true).Foreground( lipgloss.Color("4"))
)

type navItemFolder struct {
	FolderName string
	FullPath string
	Depth int
}
func (n navItemFolder) FilterValue() string {
	return n.FolderName
}
func (n navItemFolder) Title() string {
	return n.FolderName
}
func (n navItemFolder) Description() string {
	return n.FullPath
}

type itemDelegate struct{
}
func (d itemDelegate) Height() int { return 1 }
func (d itemDelegate) Spacing() int { return 0 }
func (d itemDelegate) Update(msg tea.Msg, m *list.Model) tea.Cmd { return nil }
func (d itemDelegate) Render(w io.Writer, m list.Model, index int, listItem list.Item) {
	i, ok := listItem.(list.DefaultItem)
	if !ok {
		return
	}

	desc := i.Title()
	if m.Width() > 4 && len(desc) > m.Width()-2 {
		desc = truncate.StringWithTail( desc, uint( m.Width()-2), "…")
	}

	if index == m.Index() {
		fmt.Fprint(w, fsSelectedItemStyle.Render("> " +  desc))
	} else {
		fmt.Fprint(w, fsItemStyle.Render( desc))
	}
}

func NewFolderSelectBubble( root  *archive.NodeDir, w int, h int) FolderSelectBubble {
	listDelegate := itemDelegate{}
	listModel := list.New([]list.Item{}, listDelegate, w, h - fsMavWrapOffset)
	listModel.SetShowTitle( false)
	listModel.SetShowHelp( false)
	listModel.SetShowPagination( true)
	listModel.SetShowStatusBar( false)
	// SetShowFilter
	// SetSpinner
	//  NewStatusMessage
	listModel.Title = "select folder"
	listModel.DisableQuitKeybindings()
	//log.Debugf( "FolderSelectBubble.NewFolderSelectBubble: w: %d h: %d", w, h)

	return FolderSelectBubble{
		root: root,
		list: listModel,
		delegate: listDelegate,
		width: w,
		height: h,
	}
}

type FolderSelectBubble struct {
	root  *archive.NodeDir
	list list.Model
	delegate itemDelegate
	width int
	height int
}

func (b *FolderSelectBubble) InitList() {
	var items []list.Item

	var fn func( dir *archive.NodeDir, items *[]list.Item, parentPath string, level int) 
	fn = func( dir *archive.NodeDir, items *[]list.Item, parentPath string, level int) {
		var fp string
		//log.Debugf( "FolderSelectBubble.InitList: Basename: [%s] Archname: [%s]  FullPath: %s  Depth: %d", dir.Basename, dir.Archname, fp, level)
		if level != 0 && dir.Archname != "" {
			fp = parentPath + "/" + dir.Basename
			nif := navItemFolder{ FolderName: dir.Archname, FullPath: fp, Depth: level}
			*items = append( *items, nif)
		}
		for _, s := range dir.Subdirs {
			fn( s, items, fp, level + 1)
		}
	}
	fn( b.root, &items, "", 0)

	b.list.SetItems( items)
	b.list.Select( 0)
}

func (b *FolderSelectBubble) SetSize( w int, h int) {
	b.list.SetSize( w, h)
}

func (b FolderSelectBubble) SelectedItem() ( string, error) {
			if b.IsEmpty() {
				return "", errors.New( "FolderSelectBubble.SelectedItem: empty list")
			}
			item := b.list.SelectedItem()
			if  itf, ok := item.(navItemFolder); ok {
				return itf.Title(), nil
			} else {
				log.Fatalf("FolderSelectBubble.SelectedItem illegal item: %v", item)
				return "", errors.New( "FolderSelectBubble.SelectedItem illegal item")
			}
}

func (b FolderSelectBubble) IsEmpty() bool {
	return len( b.list.Items()) == 0
}

func (b FolderSelectBubble) Init() tea.Cmd {
	return nil
}

func (b FolderSelectBubble) View() string {
	b.list.SetDelegate( b.delegate)
	/*
	return fsBubbleStyle.Render(
		lipgloss.JoinVertical(
			lipgloss.Top,
			b.list.View(),
		))
	*/
	return fsBubbleStyle.Render( b.list.View())
}

func (b FolderSelectBubble) Update(msg tea.Msg) (FolderSelectBubble, tea.Cmd) {
	var (
		cmd  tea.Cmd
		cmds []tea.Cmd
	)

	switch msg := msg.(type) {
		case tea.WindowSizeMsg:
			b.width = msg.Width
			b.height = msg.Height
			b.list.SetSize( msg.Width, msg.Height - fsMavWrapOffset)
		case tea.KeyMsg:
			b.list, cmd = b.list.Update( msg)
			cmds = append(cmds, cmd)
	}

	return b, tea.Batch( cmds...)
}

