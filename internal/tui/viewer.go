/*
Copyright © 2023 r.s.u.
This file is part of tuiarchiver
*/
package tui

import (
	"errors"
	"strings"
	"fmt"
	"bytes"
	"path/filepath"
	"io"
//	"bufio"
	"os"
	"os/exec"
	"regexp"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/bubbles/viewport"
	"github.com/alecthomas/chroma/quick"
	"github.com/charmbracelet/glamour"
	"github.com/ledongthuc/pdf"
	md "github.com/JohannesKaufmann/html-to-markdown"
	mdplugin "github.com/JohannesKaufmann/html-to-markdown/plugin"
	"github.com/muesli/reflow/wordwrap"
	"github.com/muesli/reflow/wrap"
	"github.com/gabriel-vasile/mimetype"

	archiver "github.com/mholt/archiver/v4"
	log "github.com/sirupsen/logrus"
)

const chromaFormatter = "terminal256"
const maxWordWrapSize = 1024*20
const viewportWrapOffset = 3

var (
	txtRegex = regexp.MustCompile(`^application/(xml|xhtml+xml|json|x-sh|x-httpd-php|x-subrip)$`)
	titleStyle = func() lipgloss.Style {
		b := lipgloss.RoundedBorder()
		b.Right = "├"
		return lipgloss.NewStyle().BorderStyle(b).Padding(0, 1)
	}()

	infoStyle = func() lipgloss.Style {
		b := lipgloss.RoundedBorder()
		b.Left = "┤"
		return titleStyle.Copy().BorderStyle(b)
	}()
)

type Viewer interface {
	Update( tea.Msg) ( tea.Cmd)
	View() string
	SetContent( []byte, int) error
	SetViewportContent( []byte) error
	ViewRenderedContent()
	ViewRawContent()
	GetRenderedContent() string
	GetRawContent() string
}

type TextViewer struct {
	viewport viewport.Model
	contentRaw  string
	contentRendered  string
	memberName string
	chromaFormatter string
	syntaxTheme string
	ready    bool
}

type TextDumper struct {
	textviewer TextViewer
	cols int
	noRender bool
}

type FileIsArchiveError struct {
	format archiver.Format
}

func (e FileIsArchiveError) Error() string {
	return fmt.Sprintf("file type: %s", e.format.Name())
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func NewTextDumper( syntaxTheme string, cols int, noRender bool) TextDumper {
	st := syntaxTheme
	if syntaxTheme == "auto" {
		if lipgloss.HasDarkBackground() {
			st = "monokai"
		} else {
			st = "monokailight"
		}
	}
	tv :=TextViewer{
		chromaFormatter: chromaFormatter,
		syntaxTheme: st,
		ready: true,
	}
	return TextDumper{ 
		textviewer: tv,
		cols: cols,
		noRender: noRender,
	}
}

func NewTextViewer( membername string, w, h int, syntaxTheme string) Viewer {
	st := syntaxTheme
	if syntaxTheme == "auto" {
		if lipgloss.HasDarkBackground() {
			st = "monokai"
		} else {
			st = "monokailight"
		}
	}
	return &TextViewer{
		viewport: viewport.New( w, h),
		memberName: membername,
		chromaFormatter: chromaFormatter,
		syntaxTheme: st,
		ready: true,
	}
}

func (t *TextViewer) Update( msg tea.Msg) ( tea.Cmd) {
	var cmd tea.Cmd
	t.viewport, cmd = t.viewport.Update( msg)
	return cmd
}

func (t *TextViewer) View() string {
	return fmt.Sprintf("%s\n%s\n%s", t.headerView(), t.viewport.View(), t.footerView())
}

func (t *TextViewer) ViewRenderedContent() {
	t.viewport.SetContent( t.contentRendered)
}

func (t *TextViewer) ViewRawContent() {
	t.viewport.SetContent( t.contentRaw)
}

func (t *TextViewer) GetRenderedContent() string {
	return t.contentRendered
}

func (t *TextViewer) GetRawContent() string {
	return t.contentRaw
}

func (t *TextViewer) SetViewportContent( bincontent []byte) error {
	return t.SetContent( bincontent, t.viewport.Width - viewportWrapOffset)
}

func (t *TextViewer) SetContent( bincontent []byte, w int) error {
	content := string(bincontent[:])
	switch strings.ToLower( filepath.Ext(t.memberName)) {
		case ".pdf":
			t.contentRendered, _ = t.renderPDF( bincontent, w)
			t.contentRaw = "no raw content for PDFs"
		case ".md":
			t.contentRendered, _ = t.renderMarkdown( content, w)
			t.contentRaw, _ = t.renderCode( content, w)
		case ".html":
			t.contentRendered, _ = t.renderHTML( content, w)
			t.contentRaw, _ = t.renderCode( content, w)
		case ".png", ".jpg", ".jpeg", ".tiff", ".webp", ".gif":
			t.contentRendered, _ = t.renderImage( bincontent, w - viewportWrapOffset, t.viewport.Height - 7)
			t.contentRaw = "cannot display"
		default:
			mtype := mimetype.Detect( bincontent).String()
//			log.Debugf("TextViewer.SetContent: mime: %s", mtype)
			if strings.HasPrefix( mtype, "text") || txtRegex.MatchString( mtype) {
				t.contentRendered, _ = t.renderCode( content, w)
				t.contentRaw = t.wordwrapCode( content, w)
			} else {
				t.contentRendered = fmt.Sprintf("non-text file (%s)", mtype)
				t.contentRaw = t.contentRendered
			}

	}
	return nil
}

func (t *TextViewer) renderMarkdown( content string, w int) (string, error) {
	background := "light"
	if lipgloss.HasDarkBackground() {
		background = "dark"
	}
	r, _ := glamour.NewTermRenderer(
		glamour.WithWordWrap( w),
		glamour.WithStandardStyle(background),
	)
	out, err := r.Render(content)
	if err != nil {
		return "(error styling makedown, showing original)\n\n" + content, err
	}
	return out, nil
}

func (t *TextViewer) renderHTML( content string, w int) (string, error) {
	converter := md.NewConverter("", true, nil)
	converter.Use( mdplugin.GitHubFlavored())
	markdown, err := converter.ConvertString( content)
	if err != nil {
  		return "(error styling html, showing original)\n\n" + content, err
	}
	return t.renderMarkdown( markdown, w)
}

func (t *TextViewer) renderPDF( bincontent []byte, w int) (string, error) {
	br := bytes.NewReader(bincontent)
	reader, err := pdf.NewReader( br, int64 (len( bincontent)))
	if err != nil {
		return "error reading PDF: "+err.Error(), err
	}
	buf := new(bytes.Buffer)
	buffer, err := reader.GetPlainText()
	if err != nil {
		return "error extracting text from PDF: "+err.Error(), err
	}
	_, err = buf.ReadFrom(buffer)
	if err != nil {
		return  "error reading PDF: "+err.Error(), err
	}
	var c string
	if len( buf.String()) > maxWordWrapSize {
		log.Debugf("renderPDF: %d > maxWordWrapSize", len( buf.String()))
		c = wrap.String( buf.String(), w)
	} else {
		c = wrap.String(wordwrap.String( buf.String(), w), w)
	}
	// FIXME TextDumper Height
	pdfContent := lipgloss.NewStyle().
		Width( w).
		Height(t.viewport.Height-20).
		Render( c)
	return pdfContent, nil
}

func (t *TextViewer) renderImage( bincontent []byte, w int, h int) (string, error) {

	/*
	cp, err := exec.LookPath("chafa")
	if err != nil {
		log.Errorf("renderImage: LookPath: %s", err)
		return "", err
	}
	log.Debugf("renderImage: chafa: %s", cp)
	*/

	cmd := exec.Command("chafa", "--animate=off", "--format=symbols" , "--colors=full", "--symbols=all", fmt.Sprintf("--size=%dx%d", w, h))
	//log.Debugf("renderImage: cmd: %s args: %s", cmd.Path, strings.Join( cmd.Args, " "))

	stdin, err := cmd.StdinPipe()
	if err != nil {
		log.Errorf("renderImage: %s", err)
		return "render error", err
	}

	go func() {
		defer stdin.Close()
		if n, err := stdin.Write( bincontent); err != nil {
			log.Errorf("renderImage: pipe: %s (%d bytes)", err, n)
		}
	}()

	out, err := cmd.Output()
	if err != nil {
		log.Errorf("renderImage: exec: err: %s out: %s", err, string(out))
		return "render error", err
	}
	return string( out), nil
}

func (t *TextViewer) renderCode(content string, w int) (string, error) {
	log.Trace("renderCode")
	buf := new(bytes.Buffer)
	if err := quick.Highlight(buf, content, filepath.Ext( t.memberName), t.chromaFormatter, t.syntaxTheme); err != nil {
		log.Debug("renderCode err")
		return t.wordwrapCode("(error highlighting code, showing original)\n\n" + content, w), fmt.Errorf("%w", err)
	}
	log.Trace("renderCode ok")
	return t.wordwrapCode(buf.String(), w), nil
}

func (t *TextViewer) headerView() string {
	title := titleStyle.Render(t.memberName)
	line := strings.Repeat("─", max(0, t.viewport.Width-lipgloss.Width(title)))
	return lipgloss.JoinHorizontal(lipgloss.Center, title, line)
}

func (t *TextViewer) footerView() string {
	info := infoStyle.Render(fmt.Sprintf("%3.f%%", t.viewport.ScrollPercent()*100))
	line := strings.Repeat("─", max(0, t.viewport.Width-lipgloss.Width(info)))
	return lipgloss.JoinHorizontal(lipgloss.Center, line, info)
}

func (t *TextViewer) wordwrapCode( text string, w int) string {
//	log.Debugf("wordwrapCode (len: %d)", len( text))
	if len( text) > maxWordWrapSize {
		return wrap.String( text, w)
	} else {
//		return wrap.String(wordwrap.String(text, w), w)
		f := wordwrap.NewWriter( w)
		f.Breakpoints = []rune{' ',':', ',',';','.','(','[','{',')',']','}','-','+'}
		f.Write( []byte(text))
		f.Close()
		log.Trace("wordwrapCode done")
		return wrap.String(f.String(), w)
	}
}

func (t *TextDumper) RenderInputFile( fname string) (string, error) {
	log.Debug("file: " + fname)
	var rc io.ReadCloser
	f, err := os.Open(fname)
	if err != nil {
		return fmt.Sprintf( "error opening file: %v", err), err
	}
	defer f.Close()

	format, arch_s, err := archiver.Identify(fname, f)
	if err != nil && !errors.Is(err, archiver.ErrNoMatch) {
		return fmt.Sprintf( "error identifying file: %v", err), err
	}
	log.Debugf("file format: %T", format)
	if _, ok := format.(archiver.Tar); ok {
		return fmt.Sprintf("unsupported file type: %T", format), FileIsArchiveError{ format: format}
	}
	if decom, ok := format.(archiver.Decompressor); ok {
		log.Debugf("decompressor %T", decom)
		switch decom.(type) {
			case archiver.Gz, archiver.Bz2, archiver.Zstd, archiver.Xz, archiver.Lz4:
				t.textviewer.memberName = strings.TrimSuffix(filepath.Base( fname), filepath.Ext(fname))
				rc, err = decom.OpenReader( arch_s)
				if err != nil {
					return fmt.Sprintf( "error opening file: %v", err), err
				}
			default:
				return fmt.Sprintf("unsupported file type: %T", decom), FileIsArchiveError{ format: format}
		}
	} else {
		t.textviewer.memberName = filepath.Base( fname)
		rc, err = os.Open( fname)
		if err != nil {
			return fmt.Sprintf( "error opening file: %v", err), err
		}
	}

	log.Debugf("basename %s", t.textviewer.memberName)
	defer rc.Close()
	b, err := io.ReadAll( rc)
	if err != nil {
		return fmt.Sprintf( "error reading file: %v", err), err
	}
	t.textviewer.SetContent( b, t.cols)
	if t.noRender {
		return t.textviewer.GetRawContent(), nil
	} else {
		return t.textviewer.GetRenderedContent(), nil
	}
}

