/*
Copyright © 2023 r.s.u.
This file is part of tuiarchiver
*/
package cmd

import (
	//	"bytes"
	"bufio"
//	"errors"
	"fmt"
	"os"
	"strings"

	"golang.org/x/term"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/knadh/koanf/v2"

	tui "nexus0.net/go/tuiarchiver/internal/tui"
	archive "nexus0.net/go/tuiarchiver/internal/archive"
)

type PreviewParms struct {
	Columns int
	Rows int
	MaxViewFileSize uint64
	ViewSyntaxTheme *string `koanf:"syntax-theme"`
	NoRender bool
	ArchFallBack bool `koanf:"archive-fall-back"`
}

var previewParms PreviewParms = PreviewParms{}

// previewfileCmd represents the previewfile command
var previewfileCmd = &cobra.Command{
	Use:   "previewfile",
	Short: "render one regular/compressed file to the console",
	Long: `render one regular/compressed file to the console
	not for archives, unless --archive-fall-back is passed, in which case a listing
	will be printed (default format, or according to [list] section in config file)`,
	SilenceUsage: true,
	Args: cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		k.UnmarshalWithConf("", &previewParms, koanf.UnmarshalConf{Tag: "koanf", FlatPaths: false})
		previewParms.autoTermSize()
		//log.Debugf("cols: %d rows: %d", previewParms.Columns, previewParms.Rows)
		td := tui.NewTextDumper( *previewParms.ViewSyntaxTheme, previewParms.Columns, previewParms.NoRender)
		res, err := td.RenderInputFile( args[0])
		if err == nil {
			if previewParms.Rows > 0 {
				var sb strings.Builder
				sc := bufio.NewScanner(strings.NewReader( res))
				for i := 0; sc.Scan() && i < previewParms.Rows; i++ {
					sb.WriteString( sc.Text()+"\n")
				}
				fmt.Print( sb.String())
			} else {
				fmt.Print( res)
			}
			return nil
		} else {
			switch e := err.(type) {
				case tui.FileIsArchiveError:
					if previewParms.ArchFallBack {
						sp := func(s string) *string {
							return &s
						}
						lp := archive.ListingParms {
							Columns: previewParms.Columns,
							ListType: sp("table"),
							SortOrder: sp("archive"),
							Theme: sp("simple"),
							TreeStyle: sp("simple"),
						}
						if k.Exists("list.type") {
							lp.ListType = sp( k.String("list.type"))
							log.Debugf("pf ListType: %s", *lp.ListType)
						}
						if k.Exists("list.table-style") {
							lp.Theme = sp( k.String("list.table-style"))
							log.Debugf("pf ListTheme: %s", *lp.Theme)
						}
						if k.Exists("list.tree-style") {
							lp.TreeStyle = sp( k.String("list.tree-style"))
							log.Debugf("pf TreeStyle: %s", *lp.TreeStyle)
						}
						log.Debugf("lp parm: %+v", lp)
						log.Debugf("pf parm: %+v", previewParms)
						return archive.ListArchive(args[0], lp)
					} else {
						fmt.Fprintf( os.Stderr, "file is an archive: %v\n", e)
					}
				default:
					fmt.Fprint( os.Stderr, res+"\n")
			}
			return err
		}
	},
}

func init() {
	rootCmd.AddCommand(previewfileCmd)
	previewParms.ViewSyntaxTheme = previewfileCmd.PersistentFlags().String("syntax-theme", "auto", "syntax theme for code (https://github.com/alecthomas/chroma/tree/master/styles)")
	previewfileCmd.PersistentFlags().IntVar(&previewParms.Columns, "columns", 0, "columns (0=terminal width)")
	previewfileCmd.PersistentFlags().IntVar(&previewParms.Rows, "rows", -1, "rows (0=terminal height, -1=all)")
	previewfileCmd.PersistentFlags().BoolVar(&previewParms.NoRender, "no-render", false, "output raw file")
	previewfileCmd.PersistentFlags().BoolVar(&previewParms.ArchFallBack, "archive-fall-back", false, "if file is an archive, fall back to listing")
}

func (p *PreviewParms) autoTermSize() {
	if p.Columns == 0 || p.Rows == 0 {
		width, height, err := term.GetSize( int(os.Stdout.Fd()))
		if err == nil { 
			if p.Columns == 0 {
				p.Columns = width
			}
			if p.Rows == 0 {
				p.Rows = height
			}
		} else {
			if p.Columns == 0 {
				p.Columns = 132
			}
			if p.Rows == 0 {
				p.Rows = 60
			}
		}
	}
}
