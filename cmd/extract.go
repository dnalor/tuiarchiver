/*
Copyright © 2023 r.s.u.
This file is part of tuiarchiver
*/
package cmd

import (
	"fmt"
	"os"
	"errors"

	"github.com/spf13/cobra"
	archiver "github.com/mholt/archiver/v4"

	archive "nexus0.net/go/tuiarchiver/internal/archive"
	log "github.com/sirupsen/logrus"
)

type ExtractParms struct {
	DestDir *string
	Regex *string
}

var extractParms ExtractParms = ExtractParms{}

// extractCmd represents the extract command
var extractCmd = &cobra.Command{
	Use:   "extract",
	Short: "extract from an archive",
	Long: `Extract members from an archive. If no regular expression is supplied, all members will be extracted.`,
	Args: cobra.ExactArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if *extractParms.DestDir == "" {
				if d, err := os.Getwd(); err == nil {
					extractParms.DestDir = &d
				} else {
					fmt.Fprintf( os.Stderr, "cannot get current working dir: %v\n", err)
					return err
				}
		}
		var regex *string
		if *extractParms.Regex != "" {
			regex = extractParms.Regex
		}
		return archExtract( args[0], *extractParms.DestDir, regex)
	},
}

func init() {
	rootCmd.AddCommand(extractCmd)
	extractParms.DestDir = extractCmd.PersistentFlags().String("destdir", "", "destination directory")
	extractParms.Regex = extractCmd.PersistentFlags().String("regex", "", "only extract files matching this regular expression")
	extractCmd.MarkPersistentFlagDirname( "destdir")
}

func archExtract( fname string, destdir string, regex *string) error {
	log.Debugf("archive: %s destdir: %s regex: %v", fname, destdir, regex)

	var members []string
	arch, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer arch.Close()

	format, _, err := archiver.Identify( fname, arch)
	if err != nil && !errors.Is(err, archiver.ErrNoMatch) {
		return err
	}

	if ex, ok := format.(archiver.Extractor); ok {
		r, err := archive.ExtractMembers( ex, fname, members, regex, destdir, true)
		if err == nil {
			fmt.Printf( "%s\n", r)
			return nil
		} else {
			fmt.Fprintf( os.Stderr, "error: %s\n[%v]\n", r, err)
			return err
		}
	} else {
		return errors.New( fmt.Sprintf("not a supported archive (type is: %T)", format))
	}
}
