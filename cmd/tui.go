/*
Copyright © 2023 r.s.u.
This file is part of tuiarchiver
*/
package cmd

import (
	tui "nexus0.net/go/tuiarchiver/internal/tui"

	"github.com/spf13/cobra"
	"github.com/knadh/koanf/v2"
)

var tuiParms tui.TuiParms = tui.TuiParms{}

// tuiCmd represents the tui command
var tuiCmd = &cobra.Command{
	Use:   "tui",
	Short: "launches a TUI to list/manage an archive (<f1> shows keys)",
	Long: ``,
	SilenceUsage: true,
	Args: cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		k.UnmarshalWithConf("", &tuiParms, koanf.UnmarshalConf{Tag: "koanf", FlatPaths: false})
		tuiParms.AutoTermSize()
		err := tui.Show(args[0], tuiParms)
		return err
	},
}

func init() {
	rootCmd.AddCommand(tuiCmd)

	tuiParms.DefaultExtractPath = tuiCmd.PersistentFlags().String("extract-path", "", "extract to path")
	tuiCmd.MarkFlagDirname("extract-path")
	tuiParms.ViewSyntaxTheme = tuiCmd.PersistentFlags().String("viewer-syntax-theme", "auto", "syntax theme for viewer (https://github.com/alecthomas/chroma/tree/master/styles)")
	tuiCmd.PersistentFlags().BoolVar(&tuiParms.NoIcons, "no-icons", false, "do not show icons")
	tuiCmd.PersistentFlags().UintVar(&tuiParms.Columns, "columns", 0, "columns (0=terminal width)")
	tuiCmd.PersistentFlags().UintVar(&tuiParms.Rows, "rows", 0, "rows (0=terminal height)")
}
