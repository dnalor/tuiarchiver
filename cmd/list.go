/*
Copyright © 2023 r.s.u.
This file is part of tuiarchiver
*/
package cmd

import (
	"errors"
	//"fmt"

	"github.com/spf13/cobra"
	"github.com/knadh/koanf/v2"
	log "github.com/sirupsen/logrus"

	archive "nexus0.net/go/tuiarchiver/internal/archive"
)

var listParms archive.ListingParms = archive.ListingParms{}

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "list archive content",
	Long: ``,
	SilenceUsage: true,
	Args: cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		k.UnmarshalWithConf("", &listParms, koanf.UnmarshalConf{Tag: "koanf", FlatPaths: false})
		listParms.AutoTermSize()
		if *listParms.ListType != "table" && *listParms.ListType != "tree" {
			return errors.New(" unknown ListType")
		}
		sp := func(s string) *string {
			return &s
		}
		listParms.ListType = sp( k.String("type"))
		log.Debug("cfg type is = ", k.String("type"))
		log.Debugf("lp type: %s\n", *listParms.ListType)
		err := archive.ListArchive(args[0], listParms)
		return err
	},
}

func init() {
	rootCmd.AddCommand(listCmd)

	listParms.ListType = listCmd.PersistentFlags().String("type", "table", "listing type (table/tree)")
	listParms.SortOrder = listCmd.PersistentFlags().String("sort", "archive", "sort table by (archive/name)")
	listParms.Theme = listCmd.PersistentFlags().String("table-style", "simple", "table style (dark/bright/simple/md/html)")
	listParms.TreeStyle = listCmd.PersistentFlags().String("tree-style", "simple", "tree style (simple/line/md)")
	listCmd.PersistentFlags().BoolVar(&listParms.NoIcons, "no-icons", false, "do not show icons")
	listCmd.PersistentFlags().BoolVar(&listParms.NoHeaders, "no-headers", false, "do not table headers")
	listCmd.PersistentFlags().IntVar(&listParms.Columns, "columns", 0, "columns (0=terminal width)")
}
