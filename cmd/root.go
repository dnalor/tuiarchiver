/*
Copyright © 2023 r.s.u.
This file is part of tuiarchiver
*/
package cmd

import (
	"fmt"
	"os"
//	"runtime"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
//	"github.com/spf13/viper"
	"github.com/knadh/koanf/v2"
	"github.com/knadh/koanf/parsers/toml"
	"github.com/knadh/koanf/providers/file"
	"github.com/knadh/koanf/providers/posflag"
)

var cfgFile string
var logFile string
var useDebug bool
var logLevel string

var k = koanf.New(".")

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "tuiarchiver",
	Short: "archive list/view/manage",
	Long: `TUI archiver 
-----------------------------------------------------------
(c) 2023-2025 R.S.U. / licensed under the EUPL-1.2-or-later
https://www.nexus0.net/pub/sw/tuiarchiver

- supports tar.[gz|zst|xz|bz2], zip, rar
- fullscreen TUI to view/extract members
- list archives in the terminal
- extract members from the command line
- preview files on the command line`,
	Version: "0.2.2",
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },

	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		if err := setUpLogs( useDebug, logLevel, logFile); err != nil {
			return err
		} 
		if err := setupConfig( cmd, cfgFile); err != nil {
			return err
		} 
		return nil
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $CONFIG_DIR/tuiarchiver.toml)")
	rootCmd.PersistentFlags().StringVar(&logFile, "log-file", os.TempDir()+"/tuiarchiver.log", "log file")
	rootCmd.PersistentFlags().StringVar(&logLevel, "log-level", "debug", "log level")
	rootCmd.PersistentFlags().BoolVar(&useDebug, "debug", false, "switch on debugging")
	rootCmd.MarkPersistentFlagFilename("log-file")
	rootCmd.MarkPersistentFlagFilename("config")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.

func initConfig() {
	if cfgFile == "" {
		cfgDir, err := os.UserConfigDir()
		cobra.CheckErr(err)
		cfgFile = fmt.Sprintf("%s/tuiarchiver.toml", cfgDir)
	}
}

func setupConfig( cmd *cobra.Command, cfgfile string) error {
	if err := k.Load(file.Provider(cfgfile), toml.Parser(), koanf.WithMergeFunc( createMergeFunc( cmd))); err != nil {
		log.Debugf("error loading config: %v", err)
	} else {
		log.Debug("(setupConfig) loading config file: ok")
		log.Debug("(setupConfig) type is = ", k.String("type"))
		log.Debugf("koanf map: %s", k.Sprint())
	}
	if err := k.Load(posflag.Provider(cmd.PersistentFlags(), ".", k), nil); err != nil {
		fmt.Fprintf( os.Stderr, "error parsing pflags: %v\n", err)
		return err
	} 
	return nil
}

func createMergeFunc( cmd *cobra.Command) func( a, b map[string]interface{}) error {
	return func(  a, b map[string]interface{}) error {
		var cmdName = cmd.Name()
		log.Debugf("createMergeFunc: cmdName: %s", cmdName)
		for key, val := range a {
			log.Debugf("createMergeFunc: key: %s val: %v", key, val)
			switch v := val.(type) {
				case map[string]interface{}:
					log.Debugf("createMergeFunc: type=map v: %v", v)
					if key == cmdName {
						log.Debugf("createMergeFunc: key == cmdName")
						for sk, sv := range v {
							_, ok := b[ sk]
							log.Debugf("createMergeFunc: sk: %s sv: %v match: %v", sk, sv, ok)
							if !ok {
								b[ sk] = sv
							}
						}
					} else {
						_, ok := b[ key]
						log.Debugf("createMergeFunc: key != cmdName match: %v", ok)
						if !ok {
							b[ key] = val
						}
					}
				default:
					_, ok := b[ key]
					log.Debugf("createMergeFunc: type=default match: %v", ok)
					if !ok {
						b[ key] = val
					}
			}
		}
		return nil
	}
}

func setUpLogs( debug bool, loglevel string, logpath string)  error {
	//fmt.Fprintln(os.Stderr, "Using log file:", logpath)
	var f *os.File
	var err error
	if debug {

		lv, err := log.ParseLevel( loglevel)
		if err != nil {
			return err
		}
		//log.SetLevel(log.DebugLevel)
		log.SetLevel( lv)
		if lv == log.TraceLevel {
			log.SetReportCaller(true)
		}

		if f, err = os.OpenFile(logpath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0664); err != nil {
			fmt.Fprintf( os.Stderr, "error opening file %s: %v\n", logpath, err)
			return err
		}

		log.SetOutput( f)
		log.Debugf("setting log level %v", lv)
	}
	return err
}
